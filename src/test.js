/*global testData: false */


promise.actualGet = promise.get;
promise.get = function (url) {
  'use strict';
  var p = new promise.Promise(),
          data = false;

  if (url.substring(url.length - 6) === '/state') {
    if (testData.GET && testData.GET.state) {
      data = testData.GET.state;
    } else {
      data = {};
    }
  }

  if (url.indexOf('/play') !== -1) {
    if (testData.GET && testData.GET.play) {
      data = testData.GET.play;
    } else {
      data = {};
    }
  }

  if (data !== false) {
    setTimeout(function () {
      p.done(false, JSON.stringify(data));
    }, 500);

    return p;
  } else {
    return promise.actualGet(url);
  }

};

promise.post = function (url, postData) {
  'use strict';
  var p = new promise.Promise(),
          data = {};

  if (postData.level) {
    // start
    if (testData.POST && testData.POST.level) {
      data = testData.POST.level;
    }
  }

  setTimeout(function () {
    p.done(false, JSON.stringify(data));
  }, 500);

  return p;
};