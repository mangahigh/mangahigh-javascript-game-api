/**
 * Provides some easy methods for games to communicate with Mangahigh.com
 *
 * @class MangahighGameAPI
 * @constructor
 *
 * @param {Number} gameId
 * @param {Number} userId
 *
 * @example
 *  var gameId = 1; // the Mangahigh ID of this game - will be proved by MangahighDev
 *  var userId = 1; // the Mangahigh User Id of the current user - should be proved to the game on launch
 *
 *  var api = new MangahighGameAPI(gameId, userId);
 *
 *  // all methods return a promise
 *
 *  // call on game launch
 *  // @see MangahighGameAPI.Api.init()
 *  api.init();
 *
 *  // call on start of "play"
 *  // @see MangahighGameAPI.Api.start()
 *  api.start(levelId);
 *
 *  // call on earn acheivement, or checkpoint in game
 *  // @see MangahighGameAPI.Api.update(data)
 *  api.update(data);
 *
 *  // call on finishing a level
 *  // @see MangahighGameAPI.Api.finish()
 *  api.finish(data);
 *
 *  // call when game settings change - i.e. sound, fullscreen, avatar
 *  // @see MangahighGameAPI.Api.updateSettings()
 *  api.updateSettings(data);
 *
 *  // call when game-specific gamedata changes - any persisted game data (e.g. inventory) that is not associated with
 *  // a particular playthrough or activityId
 *  // @see MangahighGameAPI.Api.updateUserGameState()
 *  api.updateUserGameState(data);
 *
 *  // call to retrieve user information using the current user's context
 *  // @see MangahighGameAPI.Api.getUser()
 *  api.getUser(userId);
 *
 *  // call when item is purchase
 *  // @see MangahighGameAPI.Api.purchaseItem()
 *  api.purchaseItem(id)
 *
 *  // retrieves a resource from mangahigh, i.e. config file
 *  // @see MangahighGameAPI.Api.getExternalResource()
 *  api.getExternalResource(url, responseType)
 *
 * @returns {Object} an object with simple functions for the game to call
 */
MangahighGameAPI = function (gameId, userId, apiBaseUrl, resourceBaseUrl) {
  'use strict';

  /**
   * The maximum number of retrys the api should do before giving up
   *
   * @property MAX_RETRY_ATTEMPTS
   * @type {Number}
   * @readOnly
   */
  var MAX_RETRY_ATTEMPTS = 3;

  /**
   * The salt to be used when creating a hash
   *
   * @property CHECKSUM_SALT
   * @type String
   * @readOnly
   */
  var CHECKSUM_SALT = '3$%d3fkkerasdfk2k3mmcs]a=-o2c455';

  /**
   * Endpoints for the API
   *
   * @property ENDPOINTS
   * @type {Object}
   * @readOnly
   */
  var ENDPOINTS = {
      init: (apiBaseUrl || '') + '/user/' + userId + '/game/' + gameId + '/state',
      aggregate: (apiBaseUrl || '') + '/user/' + userId + '/game-aggregate/' + gameId,
      start: (apiBaseUrl || '') + '/user/' + userId + '/game/' + gameId + '/play',
      update: function (gamePlayId) {
        return (apiBaseUrl || '') + '/user/' + userId + '/game/' + gameId + '/play/' + gamePlayId;
      },
      settings: (apiBaseUrl || '') + '/user/' + userId + '/game/' + gameId + '/settings',
      purchase: (apiBaseUrl || '') + '/user/' + userId + '/game/' + gameId + '/shop',
      gameLeaderboard: function (topX) {
        return (apiBaseUrl || '') + '/user/' + userId + '/game-top-x/' + gameId + (topX ? '?topX=' + topX : '');
      },
      leaderboard: function (objectiveId, topX) {
        return (apiBaseUrl || '') + '/user/' + userId + '/objective-top-x/' + objectiveId + (topX ? '?topX=' + topX : '');
      },

      gameLeaderboardOld: (apiBaseUrl || '') + '/user/' + userId + '/game-top-x/' + gameId,
      leaderboardOld: function (objectiveId) {
        return (apiBaseUrl || '') + '/user/' + userId + '/objective-leaderboard/' + objectiveId;
      },

      user: function (userId) {
        return (apiBaseUrl || '') + '/user/' + userId;
      }
    };

  // what we actually return
  // ---


  var Api = {
    /**
     * Get the initial game data
     * @method init
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     * promise done {
     *    user: {
     *      id: (int),
     *      firstName: (string),
     *      lastName: (string),
     *    },
     *
     *    school: {
     *      id: (int),
     *      name: (string),
     *      country: (string: isocode)
     *    },
     *
     *    achievements: [{
     *      activityId: (int: maps to activity_id / festivals id.. {1...5}),
     *      medal: (string: null-No Medal, B-Bronze, S-Silver, G-Gold)
     *    }],
     *
     *    balance: (int),
     *
     *    assets: {},
     *
     *    env: {},
     *
     *    settings: {
     *      ?fullScreen: (boolean),
     *      ?mute: (boolean)
     *      ?showTips: (boolean)
     *      preferences: {}
     *    },
     *
     *    gameData: {}
     * }
     */
    init: function () {
      return internalApi.init();
    },

    /**
     * Update the user's game data
     * @method updateUserGameState
     * @param {Object} data The gameData object
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     */
    updateUserGameState: function (data) {
      return internalApi.updateUserGameState(data);
    },

    /**
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     * promise done {
     *   "id":
     *     {
     *       "userId": (int),
     *       "gameId": (int)
     *     },
     *   "highScore": (int),
     *   "lastScore": (int),
     *   "timePlayed: (int)
     * }
     */
    getAggregates: function () {
      return internalApi.getAggregates();
    },

    /**
     * Start a play
     * @method start
     *
     * @param {Number|array} levelId The level which has been played
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     */
    start: function (levelId) {
      return internalApi.start(levelId);
    },

    /**
     * Update a play
     * @method update
     *
     * @param {Object} data The save data {
     *    achievements: [{
     *      activityId: (int: maps to activity_id / festivals id.. {1...5}),
     *      medal: (string: null-No Medal, B-Bronze, S-Silver, G-Gold)
     *    }],
     *
     *    balance: (int),
     *
     *    env: {},
     *
     *    gameData: {}
     * }
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     */
    update: function (data) {
      return internalApi.update(data, ACTION_UPDATE);
    },

    /**
     * Finish a play
     * @method finish
     *
     * @param {Object} data The save data {
     *    achievements: [{
     *      activityId: (int: maps to activity_id / festivals id.. {1...5}),
     *      medal: (string: null-No Medal, B-Bronze, S-Silver, G-Gold)
     *    }],
     *
     *    balance: (int),
     *
     *    env: {},
     *
     *    gameData: {}
     * }
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     */
    finish: function (data) {
      return internalApi.update(data, ACTION_FINISH);
    },

    /**
     * Update the game settings (i.e. Mute, Avatar)
     * @method updateSettings
     *
     * @param {Object} data The settings data {
     *    ?fullScreen: (boolean),
     *    ?mute: (boolean)
     *    ?showTips: (boolean)
     *    preferences: {}
     * }
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     */
    updateSettings: function (data) {
      return internalApi.updateSettings(data);
    },

    /**
     * Fetch user information relative to the current user's credentials
     * @method getUser
     *
     * @param {Number} userId
     *
     * @returns {Promise}
     */
    getUser: function (userId) {
      return internalApi.getUser(userId);
    },

    /**
     * Purchase an item from the shop
     * @method purchaseItem
     *
     * @param {Number} itemId     The purchased item id
     * @param {Number} [quantity] The quantity of purchased items, Default: 1
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     * promise done {
     *  goldBalance: (int)
     * }
     */
    purchaseItem: function (itemId, quantity) {
      return internalApi.purchaseItem(itemId, quantity);
    },

    /**
     * Get an external resource
     * @method getExternalResource
     *
     * @param {String} url          The url to fetch
     * @param {String} responseType The expected response type ('json'|'script'|null)
     *
     * @returns {Promise} Done called when the AJAX request succeeds/Script has finished loading
     * promise done Resource
     */
    getExternalResource: function (url, responseType) {
      return internalApi.getExternalResource(url, responseType);
    },

    /**
     * Create a new image
     *  - used to get round IE10 and crazy cross domain issues
     *
     * @param {String}    src
     * @param {Callback}  success
     * @param {Callback}  error
     *
     * @returns {Image}
     */
    createImage: function (src, success, error) {
      return internalApi.createImage(src, success, error);
    },

    /**
     * Fetch leaderboard data
     *
     * @param {Number}    objectiveId,
     * @param {Number}    topXResults
     *
     * @returns {Promise}
     */
    getLeaderboard: function (objectiveId, topXResults) {
      return internalApi.getLeaderboard(objectiveId, topXResults);
    },

    getLeaderboardOld: function (objectiveId) {
      return internalApi.getLeaderboardOld(objectiveId);
    },

    /**
     * Generate an object representation a Mangahigh achievement.
     *
     * @param {number} activityId
     * @param {string} medal One of either 'B', 'S', 'G'
     * @return {Object}
     */
    createAchievement: function (activityId, medal) {
      activityId = parseInt(activityId, 10);

      if (isNaN(activityId) || activityId === 0) {
        throw new Error('Invalid activityId supplied');
      }

      medal = medal.toUpperCase();

      if (medal !== 'B' && medal !== 'S' && medal !== 'G') {
        throw new Error('Invalid medal supplied');
      }

      return {
        activityId: parseInt(activityId, 10),
        medal: medal
      };
    }
  };

  // ---

  //
  /**
   * Rename promise to make things clearer for us
   *
   * @param Promise
   * @type promise.Promise
   * @readOnly
   */
  var Promise = promise.Promise,
    ajax = {
      get: promise.get,
      post: promise.post,
      put: promise.put,
      patch: promise.patch
    };

  /**
   * The update action
   *
   * @property ACTION_UPDATE
   * @type {String}
   * @readOnly
   * @static
   */
  var ACTION_UPDATE = 'update';

  /**
   * The finish action
   *
   * @property ACTION_FINISH
   * @type {String}
   * @readOnly
   * @static
   */
  var ACTION_FINISH = 'end';

  /**
   * A reponse type of json
   *
   * @property RESPONSE_JSON
   * @type String
   * @readOnly
   * @static
   */
  var RESPONSE_JSON = 'json';

  /**
   * A reponse type of a Javascript script file
   *
   * @property RESPONSE_SCRIPT
   * @type String
   * @readOnly
   * @static
   */
  var RESPONSE_SCRIPT = 'script';


  /**
   * Validates if a input is a integer
   *
   * @method isInt
   *
   * @param {Mixed} variabl
   *
   * @return {Boolean}
   */
  var isInt = function (variable) {
    return (typeof variable === 'number' && variable % 1 === 0);
  };

  // validate construction

  if (!isInt(gameId)) {
    throw new Error('An int of Game Id must be passed as the first variable');
  }

  if (userId && !isInt(userId)) {
    throw new Error('An int or null of User Id must be passed as the second variable');
  }

  // ---

  var validateBaseUrl = function (baseUrl) {
    if (!baseUrl && baseUrl !== null) {
      throw new Error('An string of the Base Url must be provided to the initialisation');
    }
  };

  validateBaseUrl(apiBaseUrl);
  validateBaseUrl(resourceBaseUrl);

  // ---

  /**
   * Wrapped in a object so we can use "this.*"
   *
   * @property GameApi
   * @type {Object}
   * @readOnly
   */
  var internalApi = {
    /**
     * A unique id representing this game play.
     * Is used so that we can correctly match Start, Update and End requests
     * in the backend
     *
     * @propery _gamePlayId
     * @type {Number}
     * @default null
     * @private
     */
    _gamePlayId: null,

    /**
     * The level id for this play. Stored here so that we don't have to keep
     * sending it from the game
     *
     * @propery _gamePlayId
     * @type {Number}
     * @default null
     * @private
     */
    _levelId: null,

    /**
     * The game play id is generated by the backend
     * If we haven't yet receieved this when we try and do an update then
     * we need to wait until this is returned
     *
     * @propery _startPromises
     * @type {Object}
     * @default {}
     * @private
     */
    _startPromises: {},

    /**
     * The index of the start promise related to this play,
     * incremented each time start is called
     *
     * @propery _gamePlayId
     * @type {Number}
     * @default 0
     * @private
     */
    _startPromiseIndex: 0,

    // ---

    init: function () {
      this._validateUser();

      var promise = new Promise();
      this._get(promise, ENDPOINTS.init, RESPONSE_JSON);
      return promise;
    },

    updateUserGameState: function (gameData) {
      this._validateUser();

      var promise = new Promise();

      if (typeof gameData !== 'object') {
        throw new Error('Must pass a gameData as an object to update user game state');
      }

      var patchData = [{
        op: 'replace',
        path: '/gameData',
        value: gameData
      }];

      this._patch(promise, ENDPOINTS.init, patchData, RESPONSE_JSON);
      return promise;
    },

    getAggregates: function () {
      this._validateUser();

      var promise = new Promise();
      this._get(promise, ENDPOINTS.aggregate, RESPONSE_JSON);
      return promise;
    },

    start: function (levelId) {
      this._validateUser();

      if (!isInt(levelId) && !Array.isArray(levelId)) {
        throw new Error('Must pass a valid levelId to start a play');
      }

      if ((Array.isArray(levelId) && levelId.length === 0)) {
        throw new Error('Must pass a valid levelId to start a play');
      }

      var promise = new Promise(),
        self = this,
        postData = {level: levelId};

      // if (this._gamePlayId) {
      //   postData.gamePlayId = this._gamePlayId;
      // }

      promise.then(function (error, data) {
        if (!error && data.gamePlayId) {
          self._gamePlayId = data.gamePlayId;
        }
      });

      this._post(promise, ENDPOINTS.start, postData, RESPONSE_JSON);

      this._levelId = levelId;

      ++this._startPromiseIndex;
      this._startPromises[this._startPromiseIndex] = promise;

      return promise;
    },

    update: function (data, action, levelId) {
      this._validateUser();

      if (!this._levelId) {
        throw new Error('Start must be called before calling update');
      }

      this._validateData(data);

      // all good - lets call the update method

      var self = this,
        promise = new Promise(),
        startPromiseIndex = this._startPromiseIndex;

      if (levelId === undefined) {
        data.level = this._levelId;
      }
      else {
        data.level = levelId;
      }

      data.action = action;

      this._startPromises[this._startPromiseIndex].then(function () {
        self._put(promise, ENDPOINTS.update(self._gamePlayId), data, RESPONSE_JSON);
        if (action === ACTION_FINISH) {
          delete self._startPromises[startPromiseIndex];
        }
      });

      return promise;
    },

    updateSettings: function (data) {
      this._validateUser();

      var promise = new Promise();
      this._put(promise, ENDPOINTS.settings, data, RESPONSE_JSON);
      return promise;
    },

    getUser: function (userId) {
      var promise = new Promise();

      this._get(promise, ENDPOINTS.user(userId), RESPONSE_JSON);
      return promise;
    },

    getExternalResource: function (url, responseType) {
      var promise = new Promise();

      if (responseType === RESPONSE_SCRIPT) {
        LazyLoad.js(url, function () {
          promise.done();
        });
      } else {
        this._get(promise, (resourceBaseUrl || '') + url, responseType);
      }

      return promise;
    },

    purchaseItem: function (itemId, quantity) {
      this._validateUser();

      if (!isInt(quantity)) {
        quantity = 1;
      }

      var promise = new Promise(),
        self = this,
        data = {
          itemId: itemId,
          quantity: quantity
        };

      self._post(promise, ENDPOINTS.purchase, data, RESPONSE_JSON);
      return promise;
    },

    /**
     * IE 10 doesn't respect CORS with new Image();
     * You get a security error when trying to getImageData()
     * This is a crazy workaround
     * - Load the image data using ajax
     * - Convert to base64
     * - add as data to the image
     * Because the image is now created locally there are no issues
     *
     * @see http://www.goodboydigital.com/ie-cross-domain-getpixeldata-hack/
     *
     * @param {String}    src
     * @param {Callback}  success
     * @param {Callback}  error
     *
     * @returns {Image}
     */
    createImage: function (src, success, error) {
      var image = new Image(),
          self = this;

      if (success) {
        image.onload = success;
      }

      if (error) {
        image.error = error;
      }

      if (navigator.appVersion.indexOf('MSIE 10') !== -1) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', src, true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function (e) {
          image.src = 'data:image/png;base64,' + self._base64ArrayBuffer(e.currentTarget.response);
        };
        xhr.send();
      } else {
        image.crossOrigin = '';
        image.src = src;
      }

      return image;
    },

    getLeaderboard: function (objectiveId, topX) {
      this._validateUser();

      var promise = new Promise(),
        self = this;

      if (!isInt(objectiveId)) {
        self._get(promise, ENDPOINTS.gameLeaderboard(topX), RESPONSE_JSON);
      } else {
        self._get(promise, ENDPOINTS.leaderboard(objectiveId, topX), RESPONSE_JSON);
      }

      return promise;
    },

    getLeaderboardOld: function (objectiveId) {
      this._validateUser();

      var promise = new Promise(),
        self = this;

      if (!isInt(objectiveId)) {
        self._get(promise, ENDPOINTS.gameLeaderboardOld, RESPONSE_JSON);
      } else {
        self._get(promise, ENDPOINTS.leaderboardOld(objectiveId), RESPONSE_JSON);
      }

      return promise;
    },

    // ---

    _validateUser: function () {
      if (!userId) {
        throw new Error('Must provide a user id to call this method');
      }
    },

    /**
     * Base 64 encode an array buffer
     *
     * @see https://gist.github.com/jonleighton/958841
     * @see http://stackoverflow.com/a/7372816
     *
     * @param {ArrayBuffer} arrayBuffer
     *
     * @returns {String}
     */
    _base64ArrayBuffer: function (arrayBuffer) {
      var base64    = '';
      var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

      var bytes         = new Uint8Array(arrayBuffer);
      var byteLength    = bytes.byteLength;
      var byteRemainder = byteLength % 3;
      var mainLength    = byteLength - byteRemainder;

      var a, b, c, d;
      var chunk;

      for (var i = 0; i < mainLength; i = i + 3) {
        chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];

        a = (chunk & 16515072) >> 18;
        b = (chunk & 258048)   >> 12;
        c = (chunk & 4032)     >>  6;
        d = chunk & 63;

        base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
      }

      if (byteRemainder === 1) {
        chunk = bytes[mainLength];

        a = (chunk & 252) >> 2;

        b = (chunk & 3)   << 4;

        base64 += encodings[a] + encodings[b] + '==';
      } else if (byteRemainder === 2) {
        chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];

        a = (chunk & 64512) >> 10;
        b = (chunk & 1008)  >>  4;

        c = (chunk & 15)    <<  2;

        base64 += encodings[a] + encodings[b] + encodings[c] + '=';
      }

      return base64;
    },

    _validateData: function (data) {
      if (!isInt(data.balance)) {
        throw new Error('Must pass a valid balance property to update a play');
      }

      if (!data.gameData || typeof data.gameData !== 'object') {
        throw new Error('Must pass a gameData object to update a play');
      }

      if (data.achievements) {
        for (var i = 0, length = data.achievements.length; i < length; ++i) {
          if (!isInt(data.achievements[i].activityId)) {
            throw new Error('Each achievement must have a valid activity id');
          }

          if (!data.achievements[i].hasOwnProperty('medal') || [null, 'B', 'S', 'G'].indexOf(data.achievements[i].medal) === -1) {
            throw new Error('Each achievement must have a valid medal');
          }
        }
      }
    },

    _get: function (p, url, responseType, attempt) {
      var self = this;

      if (typeof attempt === 'undefined') {
        attempt = 1;
      }

      ajax.get(url).then(function (error, data, xhr) {
        if (error && attempt < MAX_RETRY_ATTEMPTS) {
          self._get(p, url, responseType, ++attempt);
        } else {
          p.done(error, self._convertType(data, responseType), xhr);
        }
      });

      return p;
    },

    _post: function (p, url, postData, responseType, attempt) {
      var self = this,
        postString = JSON.stringify(postData),
        headers = {
          'X-Checksum': this._createChecksum(postString)
        };

      if (typeof attempt === 'undefined') {
        attempt = 1;
      }

      ajax.post(url, postString, headers).then(function (error, data, xhr) {
        if (error && attempt < MAX_RETRY_ATTEMPTS) {
          self._post(p, url, postData, responseType, ++attempt);
        } else {
          p.done(error, self._convertType(data, responseType), xhr);
        }
      });

      return p;
    },

    _put: function (p, url, putData, responseType, attempt) {
      var self = this,
        putString = JSON.stringify(putData),
        headers = {
          'X-Checksum': this._createChecksum(putString)
        };

      if (typeof attempt === 'undefined') {
        attempt = 1;
      }

      ajax.put(url, putString, headers).then(function (error, data, xhr) {
        if (error && attempt < MAX_RETRY_ATTEMPTS) {
          self._put(p, url, putData, responseType, ++attempt);
        } else {
          p.done(error, self._convertType(data, responseType), xhr);
        }
      });

      return p;
    },

    _patch: function (p, url, patchData, responseType, attempt) {
      var self = this,
        patchString = JSON.stringify(patchData),
        headers = {
          'X-Checksum': this._createChecksum(patchString)
        };

      if (typeof attempt === 'undefined') {
        attempt = 1;
      }

      ajax.patch(url, patchString, headers).then(function (error, data, xhr) {
        if (error && attempt < MAX_RETRY_ATTEMPTS) {
          self._patch(p, url, patchString, responseType, ++attempt);
        } else {
          p.done(error, self._convertType(data, responseType), xhr);
        }
      });

      return p;
    },

    _convertType: function (data, responseType) {
      if (responseType === RESPONSE_JSON && data) {
        return JSON.parse(data);
      }

      return data;
    },

    _createChecksum: function (data) {
      return md5(md5(data) + CHECKSUM_SALT);
    }
  };

  return Api;
};
