(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['promise'], factory);
    } else {
        // Browser globals
        root.MangahighGameAPI = factory(root.MangahighGameAPI);
    }
}(this, function (promise) {
    //= ../api.js

    return MangahighGameAPI;
}));