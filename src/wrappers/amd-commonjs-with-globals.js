(function (root, factory) {
  if (typeof module === 'object' && module.exports) {
    //todo: make this dependency reference safe
    //todo: or ideally remove the idiosyncratic promise implementation and use standard Promise class
    module.exports = factory(
      require('../vendor/promise/promise'),
      require('../vendor/md5/md5')
    );
  } else  if (typeof define === 'function' && define.amd) {
    define(['../vendor/promise/promise', '../vendor/md5/md5'], factory);
  } else {
    root.MangahighGameAPI = factory(root.MangahighGameAPI);
  }
}(this, function (promise, md5) {
  //= ../api.js

  return MangahighGameAPI;
}));