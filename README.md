# Mangahigh Game API - Javascript Edition

A wrapper for HTML5 based games to consume in order to interact with the game oriented Mangahigh.com JSON API.

## API

### `Api.init():Promise`
`init()` must be the first call made the the API - it will pass the following parameters to the next function in the promise chain:

```
Api.init.then(function (error:boolean, data:ApiResponse.Init) {
  ...
});
```

`data` will contain the following fields:
```
{
  settings:UserSettings,
  gameData:Object | null;
  balance:number;
  achievements:Object[];
  user:User;
  school:School;
}
```
---

### `Api.start(activityId:number):Promise`
Call at the beginning of what your application deems to be a play. e.g. the start of a new level. `activityId` is mandatory.

---

### `Api.update(data:Object):Promise`
Call during a game-play, after `start()` has been called and before `finish()`. Calling this is optional, but useful if game sessions are long and a use might leave before the end of 
a game but has earned an achievement that might be lost if they close the game. `data` is mandatory and has the following format:

```
{
  balance:number,
  gameData:Object,
  achievements:Achievment[]?
}
```

`balance` and `gameData` are mandatory fields

---

### `Api.finish(data:Object):Promise`
Call at the end of a level. `data` follows the same format as `Api.update()`

---

### `Api.updateSettings(data:Object):Promise`
Call this function to save user / game specific settings. `data` can contain any of the following properties:
```
{
  showTips:boolean,
  hiDef:boolean,
  fullScreen:boolean,
  mute:boolean,
  preferences:string
}
```
---

### `Api.purchaseItem(itemId:number, [quantity:number]):Promise`
Use to grant a user an item for your game. `quantity` defaults to 1 if missing. The server will perform a check to ensure
the user's `balance` is sufficient to cover the cost of the item referenced by `itemId`.

---

### `Api.getExternalResource(url:string, [responseType:string]):Promise`
This is a helper function to perform an `XMLHttpRequest`. The following response types are handled specially:

- `script`, the JS is appended to the DOM.
- `json`, the output is parsed and returned as JSON.

---

### `Api.createAchievement(activityId:number, medal:string):Achievement`
Generates an object for sending achievements to the API . The resulting object will look like:

```
{
  activityId: 1871,
  medal: 'S'
}
```


## Setup toolchain

```
npm install -g grunt-cli
npm install
```

## Build Steps

To build a distribution ready version of the library you must have grunt installed

Just run Grunt to build

```
grunt
```

This will generate 6 versions of the library, 3 minified, 3 unminified:

* mangahigh-game-api.amd-with-globals.* is an AMD compatible library which also exposes it as a global object
* mangahigh-game-api.amd-without-globals.* is an AMD compatible library which does not also expose the lib as a global
* mangahigh-game-api.* is a standard object, no AMD/CommonJS format

## Documentation
YUI docs can be generated and viewed by running the following:
```
grunt docs
```

## Tests
### Pre-Requisites
```
npm install -g testem@0.3.23
```

### Execution
```
testem ci
```
