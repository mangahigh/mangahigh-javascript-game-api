declare module Mangahigh {
    export interface GameAPI {
        new(gameId:number, userId:number, apiBaseUrl:string, resourceBaseUrl:string): GameAPI;

        /**
         * Performs a GET to get current user details
         */
        init();

        /**
         * Call on start of "play"
         */
        start(levelId:number | number[]);

        /**
         * Call on earning an achievement, or checkpoint in game
         */
        update(data:any);

        /**
         * Call on finishing a level
         */
        finish(data:any);

        /**
         * Call when game settings change - i.e. sound, fullscreen, user preference
         */
        updateSettings(data:any);

        /**
         * Call when fetching user details - ensures authorisation level is correctly set
         */
        getUser(userId:number);

        /**
         * Call when item is purchased
         * Default quantity is 1
         */
        purchaseItem(itemId:number, quantity?:number);

        /**
         * Retrieves a resource
         */
        getExternalResource(url:string, responseType?:any);

        /**
         * Get a leaderboard, limited by topXResults.
         * If objectiveId is not passed, get entire leaderboard across all objectives for that game
         */
        getLeaderboard(objectiveId?:number, topXResults?:number);

        /**
         * Create an Object representation of a Mangahigh achievement
         */
        createAchievement(activityId:number, medal:string):Achievement;
    }

    export module ApiResponse {
        export interface Init {
            settings:UserSettings,
            gameData:any;
            balance:number;
            achievements:Object[];
            user:User;
            school:School;
        }
    }

    export interface User {
        activeClassId:number;
        displayName:string
        firstName:string
        id:number
        lastName:string
        roles: {
            teacher: boolean;
            mangahighAdmin: boolean;
            organisationAdmin: boolean;
            student: boolean;
        }
        schoolId:number
    }

    export interface School {
        checked:boolean;
        city:string;
        country:string;
        curriculumId:number;
        id:number;
        locale:string;
        name:string;
        uri:string;
    }

    export interface UserSettings {
        fullScreen:boolean,
        mute:boolean,
        showTips:boolean,
        preferences:Object
    }

    export interface Achievement {
        activityId:number;
        medal:string;
    }
}

declare var MangahighGameAPI:Mangahigh.GameAPI;