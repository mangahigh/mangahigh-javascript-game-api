module.exports = function (grunt) {
  'use strict';

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    locales: ['en', 'en_GB', 'en_AU', 'en_US', 'pt_BR', 'xx_XX'],
    concat: {
      options: {
        seperator: ';\n',
        stripBanners: true
      },
      common: {
        src: [
          'vendor/promise/*.js',
          'vendor/md5/*.js',
          'vendor/lazyload/*.js',
          'src/api.js'
        ],
        dest: 'dist/mangahigh-game-api.js'
      },
      test: {
        src: [
          'vendor/promise/*.js',
          'vendor/md5/*.js',
          'vendor/lazyload/*.js',
          'src/test.js',
          'src/api.js'
        ],
        dest: 'dist/mangahigh-game-api-test.js'
      },
      dev: {
        src: [
          'vendor/promise/*.js',
          'vendor/md5/*.js',
          'vendor/lazyload/*.js',
          'src/api.js'
        ],
        dest: 'dist/mangahigh-game-api-dev.js'
      }
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      core: [
        'Gruntfile.js',
        'src/*.js'
      ]
    },
    yuidoc: {
      compile: {
        name: '<%= pkg.name %>',
        description: '<%= pkg.description %>',
        version: '<%= pkg.version %>',
        url: '<%= pkg.homepage %>',
        options: {
          paths: ['src/'],
          outdir: 'doc'
        }
      }
    },

    rig: {
      amdWithGlobals: {
        src: ['src/wrappers/amd-with-globals.js'],
        dest: 'dist/mangahigh-game-api.amd-with-globals.js'
      },
      amdWithoutGlobals: {
        src: ['src/wrappers/amd-without-globals.js'],
        dest: 'dist/mangahigh-game-api.amd-without-globals.js'
      },
      amdAndCommonJSWithGlobals: {
        src: ['src/wrappers/amd-commonjs-with-globals.js'],
        dest: 'dist/mangahigh-game-api.amd-commonjs-with-globals.js'
      }
    },

    uglify: {
      nonAmd: {
        src: '<%=concat.common.dest%>',
        dest: '<%=concat.common.dest.replace(".js",".min.js")%>'
      },

      amdWithGlobals: {
        src: '<%= rig.amdWithGlobals.dest %>',
        dest: '<%=rig.amdWithGlobals.dest.replace(".js", ".min.js")%>'
      },

      amdWithoutGlobals: {
        src: '<%= rig.amdWithoutGlobals.dest %>',
        dest: '<%=rig.amdWithoutGlobals.dest.replace(".js", ".min.js")%>'
      }
    },
    connect: {
      docs: {
        options: {
          host: '127.0.0.1',
          port: 9000,
          base: './doc',
          keepalive: true
        }
      }
    },

    clean: ['dist/'],

    open: {
      docs: {
        path: 'http://<%=connect.docs.options.host%>:<%=connect.docs.options.port%>/'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-yuidoc');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-rigger');
  grunt.loadNpmTasks('grunt-open');

  grunt.registerTask('doc:generate', 'yuidoc');
  grunt.registerTask('docs', ['doc:generate', 'open:docs', 'connect:docs']);

  grunt.registerTask('default', ['jshint', 'clean', 'concat', 'concat:test', 'concat:dev', 'rig', 'uglify']);

  grunt.registerTask('lint', 'jshint');

};
