/**
 * TODO Remove this module and use native promises
 * This github repo is no longer maintained.  This version has been patched to allow for Patch requests.
 */

(function(exports) {

    function Promise() {
        this._callbacks = [];
    }

    Promise.prototype.then = function(func, context) {
        var p;
        if (this._isdone) {
            p = func.apply(context, this.result);
        } else {
            p = new Promise();
            this._callbacks.push(function () {
                var res = func.apply(context, arguments);
                if (res && typeof res.then === 'function')
                    res.then(p.done, p);
            });
        }
        return p;
    };

    Promise.prototype.done = function() {
        this.result = arguments;
        this._isdone = true;
        for (var i = 0; i < this._callbacks.length; i++) {
            this._callbacks[i].apply(null, arguments);
        }
        this._callbacks = [];
    };

    function join(promises) {
        var p = new Promise();
        var results = [];

        if (!promises || !promises.length) {
            p.done(results);
            return p;
        }

        var numdone = 0;
        var total = promises.length;

        function notifier(i) {
            return function() {
                numdone += 1;
                results[i] = Array.prototype.slice.call(arguments);
                if (numdone === total) {
                    p.done(results);
                }
            };
        }

        for (var i = 0; i < total; i++) {
            promises[i].then(notifier(i));
        }

        return p;
    }

    function chain(funcs, args) {
        var p = new Promise();
        if (funcs.length === 0) {
            p.done.apply(p, args);
        } else {
            funcs[0].apply(null, args).then(function() {
                funcs.splice(0, 1);
                chain(funcs, arguments).then(function() {
                    p.done.apply(p, arguments);
                });
            });
        }
        return p;
    }

    /*
     * AJAX requests
     */

    function _encode(data) {
        var result = "";
        if (typeof data === "string") {
            result = data;
        } else {
            var e = encodeURIComponent;
            for (var k in data) {
                if (data.hasOwnProperty(k)) {
                    result += '&' + e(k) + '=' + e(data[k]);
                }
            }
        }
        return result;
    }

    function new_xhr() {
        var xhr;
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            try {
                xhr = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            }
        }
        return xhr;
    }


    function ajax(method, url, data, headers) {
        var p = new Promise();
        var xhr, payload;
        data = data || {};
        headers = headers || {};

        try {
            xhr = new_xhr();
        } catch (e) {
            p.done(promise.ENOXHR, "");
            return p;
        }

        payload = _encode(data);
        if (method === 'GET' && payload) {
            url += '?' + payload;
            payload = null;
        }

        xhr.open(method, url);
        xhr.withCredentials = true;
        xhr.setRequestHeader('Content-type',
                             'application/x-www-form-urlencoded');
        for (var h in headers) {
            if (headers.hasOwnProperty(h)) {
                xhr.setRequestHeader(h, headers[h]);
            }
        }

        function onTimeout() {
            xhr.abort();
            p.done(promise.ETIMEOUT, "", xhr);
        }

        var timeout = promise.ajaxTimeout;
        if (timeout) {
            var tid = setTimeout(onTimeout, timeout);
        }

        xhr.onreadystatechange = function() {
            if (timeout) {
                clearTimeout(tid);
            }
            if (xhr.readyState === 4) {
                var err = (!xhr.status ||
                           (xhr.status < 200 || xhr.status >= 300) &&
                           xhr.status !== 304);
                p.done(err, xhr.responseText, xhr);
            }
        };

        xhr.send(payload);
        return p;
    }

    function _ajaxer(method) {
        return function(url, data, headers) {
            return ajax(method, url, data, headers);
        };
    }

    var promise = {
        Promise: Promise,
        join: join,
        chain: chain,
        ajax: ajax,
        get: _ajaxer('GET'),
        post: _ajaxer('POST'),
        put: _ajaxer('PUT'),
        del: _ajaxer('DELETE'),
        patch: _ajaxer('PATCH'),

        /* Error codes */
        ENOXHR: 1,
        ETIMEOUT: 2,

        /**
         * Configuration parameter: time in milliseconds after which a
         * pending AJAX request is considered unresponsive and is
         * aborted. Useful to deal with bad connectivity (e.g. on a
         * mobile network). A 0 value disables AJAX timeouts.
         *
         * Aborted requests resolve the promise with a ETIMEOUT error
         * code.
         */
        ajaxTimeout: 0
    };

    if (typeof define === 'function' && define.amd) {
        /* AMD support */
        define(function() {
            return promise;
        });
    } else {
        exports.promise = promise;
    }

})(this);

;(function(root, undefined) {
  'use strict';

  var NODE_JS = typeof(module) != 'undefined';
  if(NODE_JS) {
    root = global;
    if(root.JS_MD5_TEST) {
      root.navigator = { userAgent: 'Firefox'};
    }
  }
  var FIREFOX = (root.JS_MD5_TEST || !NODE_JS) && navigator.userAgent.indexOf('Firefox') != -1;
  var ARRAY_BUFFER = !root.JS_MD5_TEST && typeof(ArrayBuffer) != 'undefined';
  var HEX_CHARS = '0123456789abcdef'.split('');
  var EXTRA = [128, 32768, 8388608, -2147483648];
  var SHIFT = [0, 8, 16, 24];

  var blocks = [], buffer8;
  if(ARRAY_BUFFER) {
    var buffer = new ArrayBuffer(68);
    buffer8 = new Uint8Array(buffer);
    blocks = new Uint32Array(buffer);
  }

  var md5 = function(message) {
    var notString = typeof(message) != 'string';
    if(notString && message.constructor == ArrayBuffer) {
      message = new Uint8Array(message);
    }

    var h0, h1, h2, h3, a, b, c, d, bc, da, code, first = true, end = false,
      index = 0, i, start = 0, bytes = 0, length = message.length;
    blocks[16] = 0;
    do {
      blocks[0] = blocks[16];
      blocks[16] = blocks[1] = blocks[2] = blocks[3] =
        blocks[4] = blocks[5] = blocks[6] = blocks[7] =
          blocks[8] = blocks[9] = blocks[10] = blocks[11] =
            blocks[12] = blocks[13] = blocks[14] = blocks[15] = 0;
      if(notString) {
        if(ARRAY_BUFFER) {
          for (i = start;index < length && i < 64; ++index) {
            buffer8[i++] = message[index];
          }
        } else {
          for (i = start;index < length && i < 64; ++index) {
            blocks[i >> 2] |= message[index] << SHIFT[i++ & 3];
          }
        }
      } else {
        if(ARRAY_BUFFER) {
          for (i = start;index < length && i < 64; ++index) {
            code = message.charCodeAt(index);
            if (code < 0x80) {
              buffer8[i++] = code;
            } else if (code < 0x800) {
              buffer8[i++] = 0xc0 | (code >> 6);
              buffer8[i++] = 0x80 | (code & 0x3f);
            } else if (code < 0xd800 || code >= 0xe000) {
              buffer8[i++] = 0xe0 | (code >> 12);
              buffer8[i++] = 0x80 | ((code >> 6) & 0x3f);
              buffer8[i++] = 0x80 | (code & 0x3f);
            } else {
              code = 0x10000 + (((code & 0x3ff) << 10) | (message.charCodeAt(++index) & 0x3ff));
              buffer8[i++] = 0xf0 | (code >> 18);
              buffer8[i++] = 0x80 | ((code >> 12) & 0x3f);
              buffer8[i++] = 0x80 | ((code >> 6) & 0x3f);
              buffer8[i++] = 0x80 | (code & 0x3f);
            }
          }
        } else {
          for (i = start;index < length && i < 64; ++index) {
            code = message.charCodeAt(index);
            if (code < 0x80) {
              blocks[i >> 2] |= code << SHIFT[i++ & 3];
            } else if (code < 0x800) {
              blocks[i >> 2] |= (0xc0 | (code >> 6)) << SHIFT[i++ & 3];
              blocks[i >> 2] |= (0x80 | (code & 0x3f)) << SHIFT[i++ & 3];
            } else if (code < 0xd800 || code >= 0xe000) {
              blocks[i >> 2] |= (0xe0 | (code >> 12)) << SHIFT[i++ & 3];
              blocks[i >> 2] |= (0x80 | ((code >> 6) & 0x3f)) << SHIFT[i++ & 3];
              blocks[i >> 2] |= (0x80 | (code & 0x3f)) << SHIFT[i++ & 3];
            } else {
              code = 0x10000 + (((code & 0x3ff) << 10) | (message.charCodeAt(++index) & 0x3ff));
              blocks[i >> 2] |= (0xf0 | (code >> 18)) << SHIFT[i++ & 3];
              blocks[i >> 2] |= (0x80 | ((code >> 12) & 0x3f)) << SHIFT[i++ & 3];
              blocks[i >> 2] |= (0x80 | ((code >> 6) & 0x3f)) << SHIFT[i++ & 3];
              blocks[i >> 2] |= (0x80 | (code & 0x3f)) << SHIFT[i++ & 3];
            }
          }
        }
      }
      bytes += i - start;
      start = i - 64;
      if(index == length) {
        blocks[i >> 2] |= EXTRA[i & 3];
        ++index;
      }
      if(index > length && i < 56) {
        blocks[14] = bytes << 3;
        end = true;
      }

      if(first) {
        a = blocks[0] - 680876937;
        a = (a << 7 | a >>> 25) - 271733879 << 0;
        d = (-1732584194 ^ a & 2004318071) + blocks[1] - 117830708;
        d = (d << 12 | d >>> 20) + a << 0;
        c = (-271733879 ^ (d & (a ^ -271733879))) + blocks[2] - 1126478375;
        c = (c << 17 | c >>> 15) + d << 0;
        b = (a ^ (c & (d ^ a))) + blocks[3] - 1316259209;
        b = (b << 22 | b >>> 10) + c << 0;
      } else {
        a = h0;
        b = h1;
        c = h2;
        d = h3;
        a += (d ^ (b & (c ^ d))) + blocks[0] - 680876936;
        a = (a << 7 | a >>> 25) + b << 0;
        d += (c ^ (a & (b ^ c))) + blocks[1] - 389564586;
        d = (d << 12 | d >>> 20) + a << 0;
        c += (b ^ (d & (a ^ b))) + blocks[2] + 606105819;
        c = (c << 17 | c >>> 15) + d << 0;
        b += (a ^ (c & (d ^ a))) + blocks[3] - 1044525330;
        b = (b << 22 | b >>> 10) + c << 0;
      }

      a += (d ^ (b & (c ^ d))) + blocks[4] - 176418897;
      a = (a << 7 | a >>> 25) + b << 0;
      d += (c ^ (a & (b ^ c))) + blocks[5] + 1200080426;
      d = (d << 12 | d >>> 20) + a << 0;
      c += (b ^ (d & (a ^ b))) + blocks[6] - 1473231341;
      c = (c << 17 | c >>> 15) + d << 0;
      b += (a ^ (c & (d ^ a))) + blocks[7] - 45705983;
      b = (b << 22 | b >>> 10) + c << 0;
      a += (d ^ (b & (c ^ d))) + blocks[8] + 1770035416;
      a = (a << 7 | a >>> 25) + b << 0;
      d += (c ^ (a & (b ^ c))) + blocks[9] - 1958414417;
      d = (d << 12 | d >>> 20) + a << 0;
      c += (b ^ (d & (a ^ b))) + blocks[10] - 42063;
      c = (c << 17 | c >>> 15) + d << 0;
      b += (a ^ (c & (d ^ a))) + blocks[11] - 1990404162;
      b = (b << 22 | b >>> 10) + c << 0;
      a += (d ^ (b & (c ^ d))) + blocks[12] + 1804603682;
      a = (a << 7 | a >>> 25) + b << 0;
      d += (c ^ (a & (b ^ c))) + blocks[13] - 40341101;
      d = (d << 12 | d >>> 20) + a << 0;
      c += (b ^ (d & (a ^ b))) + blocks[14] - 1502002290;
      c = (c << 17 | c >>> 15) + d << 0;
      b += (a ^ (c & (d ^ a))) + blocks[15] + 1236535329;
      b = (b << 22 | b >>> 10) + c << 0;
      a += (c ^ (d & (b ^ c))) + blocks[1] - 165796510;
      a = (a << 5 | a >>> 27) + b << 0;
      d += (b ^ (c & (a ^ b))) + blocks[6] - 1069501632;
      d = (d << 9 | d >>> 23) + a << 0;
      c += (a ^ (b & (d ^ a))) + blocks[11] + 643717713;
      c = (c << 14 | c >>> 18) + d << 0;
      b += (d ^ (a & (c ^ d))) + blocks[0] - 373897302;
      b = (b << 20 | b >>> 12) + c << 0;
      a += (c ^ (d & (b ^ c))) + blocks[5] - 701558691;
      a = (a << 5 | a >>> 27) + b << 0;
      d += (b ^ (c & (a ^ b))) + blocks[10] + 38016083;
      d = (d << 9 | d >>> 23) + a << 0;
      c += (a ^ (b & (d ^ a))) + blocks[15] - 660478335;
      c = (c << 14 | c >>> 18) + d << 0;
      b += (d ^ (a & (c ^ d))) + blocks[4] - 405537848;
      b = (b << 20 | b >>> 12) + c << 0;
      a += (c ^ (d & (b ^ c))) + blocks[9] + 568446438;
      a = (a << 5 | a >>> 27) + b << 0;
      d += (b ^ (c & (a ^ b))) + blocks[14] - 1019803690;
      d = (d << 9 | d >>> 23) + a << 0;
      c += (a ^ (b & (d ^ a))) + blocks[3] - 187363961;
      c = (c << 14 | c >>> 18) + d << 0;
      b += (d ^ (a & (c ^ d))) + blocks[8] + 1163531501;
      b = (b << 20 | b >>> 12) + c << 0;
      a += (c ^ (d & (b ^ c))) + blocks[13] - 1444681467;
      a = (a << 5 | a >>> 27) + b << 0;
      d += (b ^ (c & (a ^ b))) + blocks[2] - 51403784;
      d = (d << 9 | d >>> 23) + a << 0;
      c += (a ^ (b & (d ^ a))) + blocks[7] + 1735328473;
      c = (c << 14 | c >>> 18) + d << 0;
      b += (d ^ (a & (c ^ d))) + blocks[12] - 1926607734;
      b = (b << 20 | b >>> 12) + c << 0;
      bc = b ^ c;
      a += (bc ^ d) + blocks[5] - 378558;
      a = (a << 4 | a >>> 28) + b << 0;
      d += (bc ^ a) + blocks[8] - 2022574463;
      d = (d << 11 | d >>> 21) + a << 0;
      da = d ^ a;
      c += (da ^ b) + blocks[11] + 1839030562;
      c = (c << 16 | c >>> 16) + d << 0;
      b += (da ^ c) + blocks[14] - 35309556;
      b = (b << 23 | b >>> 9) + c << 0;
      bc = b ^ c;
      a += (bc ^ d) + blocks[1] - 1530992060;
      a = (a << 4 | a >>> 28) + b << 0;
      d += (bc ^ a) + blocks[4] + 1272893353;
      d = (d << 11 | d >>> 21) + a << 0;
      da = d ^ a;
      c += (da ^ b) + blocks[7] - 155497632;
      c = (c << 16 | c >>> 16) + d << 0;
      b += (da ^ c) + blocks[10] - 1094730640;
      b = (b << 23 | b >>> 9) + c << 0;
      bc = b ^ c;
      a += (bc ^ d) + blocks[13] + 681279174;
      a = (a << 4 | a >>> 28) + b << 0;
      d += (bc ^ a) + blocks[0] - 358537222;
      d = (d << 11 | d >>> 21) + a << 0;
      da = d ^ a;
      c += (da ^ b) + blocks[3] - 722521979;
      c = (c << 16 | c >>> 16) + d << 0;
      b += (da ^ c) + blocks[6] + 76029189;
      b = (b << 23 | b >>> 9) + c << 0;
      bc = b ^ c;
      a += (bc ^ d) + blocks[9] - 640364487;
      a = (a << 4 | a >>> 28) + b << 0;
      d += (bc ^ a) + blocks[12] - 421815835;
      d = (d << 11 | d >>> 21) + a << 0;
      da = d ^ a;
      c += (da ^ b) + blocks[15] + 530742520;
      c = (c << 16 | c >>> 16) + d << 0;
      b += (da ^ c) + blocks[2] - 995338651;
      b = (b << 23 | b >>> 9) + c << 0;
      a += (c ^ (b | ~d)) + blocks[0] - 198630844;
      a = (a << 6 | a >>> 26) + b << 0;
      d += (b ^ (a | ~c)) + blocks[7] + 1126891415;
      d = (d << 10 | d >>> 22) + a << 0;
      c += (a ^ (d | ~b)) + blocks[14] - 1416354905;
      c = (c << 15 | c >>> 17) + d << 0;
      b += (d ^ (c | ~a)) + blocks[5] - 57434055;
      b = (b << 21 | b >>> 11) + c << 0;
      a += (c ^ (b | ~d)) + blocks[12] + 1700485571;
      a = (a << 6 | a >>> 26) + b << 0;
      d += (b ^ (a | ~c)) + blocks[3] - 1894986606;
      d = (d << 10 | d >>> 22) + a << 0;
      c += (a ^ (d | ~b)) + blocks[10] - 1051523;
      c = (c << 15 | c >>> 17) + d << 0;
      b += (d ^ (c | ~a)) + blocks[1] - 2054922799;
      b = (b << 21 | b >>> 11) + c << 0;
      a += (c ^ (b | ~d)) + blocks[8] + 1873313359;
      a = (a << 6 | a >>> 26) + b << 0;
      d += (b ^ (a | ~c)) + blocks[15] - 30611744;
      d = (d << 10 | d >>> 22) + a << 0;
      c += (a ^ (d | ~b)) + blocks[6] - 1560198380;
      c = (c << 15 | c >>> 17) + d << 0;
      b += (d ^ (c | ~a)) + blocks[13] + 1309151649;
      b = (b << 21 | b >>> 11) + c << 0;
      a += (c ^ (b | ~d)) + blocks[4] - 145523070;
      a = (a << 6 | a >>> 26) + b << 0;
      d += (b ^ (a | ~c)) + blocks[11] - 1120210379;
      d = (d << 10 | d >>> 22) + a << 0;
      c += (a ^ (d | ~b)) + blocks[2] + 718787259;
      c = (c << 15 | c >>> 17) + d << 0;
      b += (d ^ (c | ~a)) + blocks[9] - 343485551;
      b = (b << 21 | b >>> 11) + c << 0;

      if(first) {
        h0 = a + 1732584193 << 0;
        h1 = b - 271733879 << 0;
        h2 = c - 1732584194 << 0;
        h3 = d + 271733878 << 0;
        first = false;
      } else {
        h0 = h0 + a << 0;
        h1 = h1 + b << 0;
        h2 = h2 + c << 0;
        h3 = h3 + d << 0;
      }
    } while(!end);

    if(FIREFOX) {
      var hex = HEX_CHARS[(h0 >> 4) & 0x0F] + HEX_CHARS[h0 & 0x0F];
      hex += HEX_CHARS[(h0 >> 12) & 0x0F] + HEX_CHARS[(h0 >> 8) & 0x0F];
      hex += HEX_CHARS[(h0 >> 20) & 0x0F] + HEX_CHARS[(h0 >> 16) & 0x0F];
      hex += HEX_CHARS[(h0 >> 28) & 0x0F] + HEX_CHARS[(h0 >> 24) & 0x0F];
      hex += HEX_CHARS[(h1 >> 4) & 0x0F] + HEX_CHARS[h1 & 0x0F];
      hex += HEX_CHARS[(h1 >> 12) & 0x0F] + HEX_CHARS[(h1 >> 8) & 0x0F];
      hex += HEX_CHARS[(h1 >> 20) & 0x0F] + HEX_CHARS[(h1 >> 16) & 0x0F];
      hex += HEX_CHARS[(h1 >> 28) & 0x0F] + HEX_CHARS[(h1 >> 24) & 0x0F];
      hex += HEX_CHARS[(h2 >> 4) & 0x0F] + HEX_CHARS[h2 & 0x0F];
      hex += HEX_CHARS[(h2 >> 12) & 0x0F] + HEX_CHARS[(h2 >> 8) & 0x0F];
      hex += HEX_CHARS[(h2 >> 20) & 0x0F] + HEX_CHARS[(h2 >> 16) & 0x0F];
      hex += HEX_CHARS[(h2 >> 28) & 0x0F] + HEX_CHARS[(h2 >> 24) & 0x0F];
      hex += HEX_CHARS[(h3 >> 4) & 0x0F] + HEX_CHARS[h3 & 0x0F];
      hex += HEX_CHARS[(h3 >> 12) & 0x0F] + HEX_CHARS[(h3 >> 8) & 0x0F];
      hex += HEX_CHARS[(h3 >> 20) & 0x0F] + HEX_CHARS[(h3 >> 16) & 0x0F];
      hex += HEX_CHARS[(h3 >> 28) & 0x0F] + HEX_CHARS[(h3 >> 24) & 0x0F];
      return hex;
    } else {
      return HEX_CHARS[(h0 >> 4) & 0x0F] + HEX_CHARS[h0 & 0x0F] +
        HEX_CHARS[(h0 >> 12) & 0x0F] + HEX_CHARS[(h0 >> 8) & 0x0F] +
        HEX_CHARS[(h0 >> 20) & 0x0F] + HEX_CHARS[(h0 >> 16) & 0x0F] +
        HEX_CHARS[(h0 >> 28) & 0x0F] + HEX_CHARS[(h0 >> 24) & 0x0F] +
        HEX_CHARS[(h1 >> 4) & 0x0F] + HEX_CHARS[h1 & 0x0F] +
        HEX_CHARS[(h1 >> 12) & 0x0F] + HEX_CHARS[(h1 >> 8) & 0x0F] +
        HEX_CHARS[(h1 >> 20) & 0x0F] + HEX_CHARS[(h1 >> 16) & 0x0F] +
        HEX_CHARS[(h1 >> 28) & 0x0F] + HEX_CHARS[(h1 >> 24) & 0x0F] +
        HEX_CHARS[(h2 >> 4) & 0x0F] + HEX_CHARS[h2 & 0x0F] +
        HEX_CHARS[(h2 >> 12) & 0x0F] + HEX_CHARS[(h2 >> 8) & 0x0F] +
        HEX_CHARS[(h2 >> 20) & 0x0F] + HEX_CHARS[(h2 >> 16) & 0x0F] +
        HEX_CHARS[(h2 >> 28) & 0x0F] + HEX_CHARS[(h2 >> 24) & 0x0F] +
        HEX_CHARS[(h3 >> 4) & 0x0F] + HEX_CHARS[h3 & 0x0F] +
        HEX_CHARS[(h3 >> 12) & 0x0F] + HEX_CHARS[(h3 >> 8) & 0x0F] +
        HEX_CHARS[(h3 >> 20) & 0x0F] + HEX_CHARS[(h3 >> 16) & 0x0F] +
        HEX_CHARS[(h3 >> 28) & 0x0F] + HEX_CHARS[(h3 >> 24) & 0x0F];
    }
  };

  if(!root.JS_MD5_TEST && NODE_JS) {
    var crypto = require('crypto');
    var Buffer = require('buffer').Buffer;

    module.exports = function(message) {
      if(typeof(message) == 'string') {
        if(message.length <= 80) {
          return md5(message);
        } else if(message.length <= 183 && !/[^\x00-\x7F]/.test(message)) {
          return md5(message);
        }
        return crypto.createHash('md5').update(message, 'utf8').digest('hex');
      }
      if(message.constructor == ArrayBuffer) {
        message = new Uint8Array(message);
      }
      if(message.length <= 370) {
        return md5(message);
      }
      return crypto.createHash('md5').update(new Buffer(message)).digest('hex');
    };
  } else if(root) {
    root.md5 = md5;
  }
}(this));
/**
LazyLoad makes it easy and painless to lazily load one or more external
JavaScript or CSS files on demand either during or after the rendering of a web
page.

Supported browsers include Firefox 2+, IE6+, Safari 3+ (including Mobile
Safari), Google Chrome, and Opera 9+. Other browsers may or may not work and
are not officially supported.

Visit https://github.com/rgrove/lazyload/ for more info.

Copyright (c) 2011 Ryan Grove <ryan@wonko.com>
All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the 'Software'), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

@module lazyload
@class LazyLoad
@static
*/

LazyLoad = (function (doc) {
  // -- Private Variables ------------------------------------------------------

  // User agent and feature test information.
  var env,

  // Reference to the <head> element (populated lazily).
  head,

  // Requests currently in progress, if any.
  pending = {},

  // Number of times we've polled to check whether a pending stylesheet has
  // finished loading. If this gets too high, we're probably stalled.
  pollCount = 0,

  // Queued requests.
  queue = {css: [], js: []},

  // Reference to the browser's list of stylesheets.
  styleSheets = doc.styleSheets;

  // -- Private Methods --------------------------------------------------------

  /**
  Creates and returns an HTML element with the specified name and attributes.

  @method createNode
  @param {String} name element name
  @param {Object} attrs name/value mapping of element attributes
  @return {HTMLElement}
  @private
  */
  function createNode(name, attrs) {
    var node = doc.createElement(name), attr;

    for (attr in attrs) {
      if (attrs.hasOwnProperty(attr)) {
        node.setAttribute(attr, attrs[attr]);
      }
    }

    return node;
  }

  /**
  Called when the current pending resource of the specified type has finished
  loading. Executes the associated callback (if any) and loads the next
  resource in the queue.

  @method finish
  @param {String} type resource type ('css' or 'js')
  @private
  */
  function finish(type) {
    var p = pending[type],
        callback,
        urls;

    if (p) {
      callback = p.callback;
      urls     = p.urls;

      urls.shift();
      pollCount = 0;

      // If this is the last of the pending URLs, execute the callback and
      // start the next request in the queue (if any).
      if (!urls.length) {
        callback && callback.call(p.context, p.obj);
        pending[type] = null;
        queue[type].length && load(type);
      }
    }
  }

  /**
  Populates the <code>env</code> variable with user agent and feature test
  information.

  @method getEnv
  @private
  */
  function getEnv() {
    var ua = navigator.userAgent;

    env = {
      // True if this browser supports disabling async mode on dynamically
      // created script nodes. See
      // http://wiki.whatwg.org/wiki/Dynamic_Script_Execution_Order
      async: doc.createElement('script').async === true
    };

    (env.webkit = /AppleWebKit\//.test(ua))
      || (env.ie = /MSIE|Trident/.test(ua))
      || (env.opera = /Opera/.test(ua))
      || (env.gecko = /Gecko\//.test(ua))
      || (env.unknown = true);
  }

  /**
  Loads the specified resources, or the next resource of the specified type
  in the queue if no resources are specified. If a resource of the specified
  type is already being loaded, the new request will be queued until the
  first request has been finished.

  When an array of resource URLs is specified, those URLs will be loaded in
  parallel if it is possible to do so while preserving execution order. All
  browsers support parallel loading of CSS, but only Firefox and Opera
  support parallel loading of scripts. In other browsers, scripts will be
  queued and loaded one at a time to ensure correct execution order.

  @method load
  @param {String} type resource type ('css' or 'js')
  @param {String|Array} urls (optional) URL or array of URLs to load
  @param {Function} callback (optional) callback function to execute when the
    resource is loaded
  @param {Object} obj (optional) object to pass to the callback function
  @param {Object} context (optional) if provided, the callback function will
    be executed in this object's context
  @private
  */
  function load(type, urls, callback, obj, context) {
    var _finish = function () { finish(type); },
        isCSS   = type === 'css',
        nodes   = [],
        i, len, node, p, pendingUrls, url;

    env || getEnv();

    if (urls) {
      // If urls is a string, wrap it in an array. Otherwise assume it's an
      // array and create a copy of it so modifications won't be made to the
      // original.
      urls = typeof urls === 'string' ? [urls] : urls.concat();

      // Create a request object for each URL. If multiple URLs are specified,
      // the callback will only be executed after all URLs have been loaded.
      //
      // Sadly, Firefox and Opera are the only browsers capable of loading
      // scripts in parallel while preserving execution order. In all other
      // browsers, scripts must be loaded sequentially.
      //
      // All browsers respect CSS specificity based on the order of the link
      // elements in the DOM, regardless of the order in which the stylesheets
      // are actually downloaded.
      if (isCSS || env.async || env.gecko || env.opera) {
        // Load in parallel.
        queue[type].push({
          urls    : urls,
          callback: callback,
          obj     : obj,
          context : context
        });
      } else {
        // Load sequentially.
        for (i = 0, len = urls.length; i < len; ++i) {
          queue[type].push({
            urls    : [urls[i]],
            callback: i === len - 1 ? callback : null, // callback is only added to the last URL
            obj     : obj,
            context : context
          });
        }
      }
    }

    // If a previous load request of this type is currently in progress, we'll
    // wait our turn. Otherwise, grab the next item in the queue.
    if (pending[type] || !(p = pending[type] = queue[type].shift())) {
      return;
    }

    head || (head = doc.head || doc.getElementsByTagName('head')[0]);
    pendingUrls = p.urls;

    for (i = 0, len = pendingUrls.length; i < len; ++i) {
      url = pendingUrls[i];

      if (isCSS) {
          node = env.gecko ? createNode('style') : createNode('link', {
            href: url,
            rel : 'stylesheet'
          });
      } else {
        node = createNode('script', {src: url});
        node.async = false;
      }

      node.className = 'lazyload';
      node.setAttribute('charset', 'utf-8');

      if (env.ie && !isCSS && 'onreadystatechange' in node && !('draggable' in node)) {
        node.onreadystatechange = function () {
          if (/loaded|complete/.test(node.readyState)) {
            node.onreadystatechange = null;
            _finish();
          }
        };
      } else if (isCSS && (env.gecko || env.webkit)) {
        // Gecko and WebKit don't support the onload event on link nodes.
        if (env.webkit) {
          // In WebKit, we can poll for changes to document.styleSheets to
          // figure out when stylesheets have loaded.
          p.urls[i] = node.href; // resolve relative URLs (or polling won't work)
          pollWebKit();
        } else {
          // In Gecko, we can import the requested URL into a <style> node and
          // poll for the existence of node.sheet.cssRules. Props to Zach
          // Leatherman for calling my attention to this technique.
          node.innerHTML = '@import "' + url + '";';
          pollGecko(node);
        }
      } else {
        node.onload = node.onerror = _finish;
      }

      nodes.push(node);
    }

    for (i = 0, len = nodes.length; i < len; ++i) {
      head.appendChild(nodes[i]);
    }
  }

  /**
  Begins polling to determine when the specified stylesheet has finished loading
  in Gecko. Polling stops when all pending stylesheets have loaded or after 10
  seconds (to prevent stalls).

  Thanks to Zach Leatherman for calling my attention to the @import-based
  cross-domain technique used here, and to Oleg Slobodskoi for an earlier
  same-domain implementation. See Zach's blog for more details:
  http://www.zachleat.com/web/2010/07/29/load-css-dynamically/

  @method pollGecko
  @param {HTMLElement} node Style node to poll.
  @private
  */
  function pollGecko(node) {
    var hasRules;

    try {
      // We don't really need to store this value or ever refer to it again, but
      // if we don't store it, Closure Compiler assumes the code is useless and
      // removes it.
      hasRules = !!node.sheet.cssRules;
    } catch (ex) {
      // An exception means the stylesheet is still loading.
      pollCount += 1;

      if (pollCount < 200) {
        setTimeout(function () { pollGecko(node); }, 50);
      } else {
        // We've been polling for 10 seconds and nothing's happened. Stop
        // polling and finish the pending requests to avoid blocking further
        // requests.
        hasRules && finish('css');
      }

      return;
    }

    // If we get here, the stylesheet has loaded.
    finish('css');
  }

  /**
  Begins polling to determine when pending stylesheets have finished loading
  in WebKit. Polling stops when all pending stylesheets have loaded or after 10
  seconds (to prevent stalls).

  @method pollWebKit
  @private
  */
  function pollWebKit() {
    var css = pending.css, i;

    if (css) {
      i = styleSheets.length;

      // Look for a stylesheet matching the pending URL.
      while (--i >= 0) {
        if (styleSheets[i].href === css.urls[0]) {
          finish('css');
          break;
        }
      }

      pollCount += 1;

      if (css) {
        if (pollCount < 200) {
          setTimeout(pollWebKit, 50);
        } else {
          // We've been polling for 10 seconds and nothing's happened, which may
          // indicate that the stylesheet has been removed from the document
          // before it had a chance to load. Stop polling and finish the pending
          // request to prevent blocking further requests.
          finish('css');
        }
      }
    }
  }

  return {

    /**
    Requests the specified CSS URL or URLs and executes the specified
    callback (if any) when they have finished loading. If an array of URLs is
    specified, the stylesheets will be loaded in parallel and the callback
    will be executed after all stylesheets have finished loading.

    @method css
    @param {String|Array} urls CSS URL or array of CSS URLs to load
    @param {Function} callback (optional) callback function to execute when
      the specified stylesheets are loaded
    @param {Object} obj (optional) object to pass to the callback function
    @param {Object} context (optional) if provided, the callback function
      will be executed in this object's context
    @static
    */
    css: function (urls, callback, obj, context) {
      load('css', urls, callback, obj, context);
    },

    /**
    Requests the specified JavaScript URL or URLs and executes the specified
    callback (if any) when they have finished loading. If an array of URLs is
    specified and the browser supports it, the scripts will be loaded in
    parallel and the callback will be executed after all scripts have
    finished loading.

    Currently, only Firefox and Opera support parallel loading of scripts while
    preserving execution order. In other browsers, scripts will be
    queued and loaded one at a time to ensure correct execution order.

    @method js
    @param {String|Array} urls JS URL or array of JS URLs to load
    @param {Function} callback (optional) callback function to execute when
      the specified scripts are loaded
    @param {Object} obj (optional) object to pass to the callback function
    @param {Object} context (optional) if provided, the callback function
      will be executed in this object's context
    @static
    */
    js: function (urls, callback, obj, context) {
      load('js', urls, callback, obj, context);
    }

  };
})(this.document);


MangahighGameAPI = function (gameId, userId, apiBaseUrl, resourceBaseUrl) {
  'use strict';

  /**
   * The maximum number of retrys the api should do before giving up
   *
   * @property MAX_RETRY_ATTEMPTS
   * @type {Number}
   * @readOnly
   */
  var MAX_RETRY_ATTEMPTS = 3;

  /**
   * The salt to be used when creating a hash
   *
   * @property CHECKSUM_SALT
   * @type String
   * @readOnly
   */
  var CHECKSUM_SALT = '3$%d3fkkerasdfk2k3mmcs]a=-o2c455';

  /**
   * Endpoints for the API
   *
   * @property ENDPOINTS
   * @type {Object}
   * @readOnly
   */
  var ENDPOINTS = {
      init: (apiBaseUrl || '') + '/user/' + userId + '/game/' + gameId + '/state',
      aggregate: (apiBaseUrl || '') + '/user/' + userId + '/game-aggregate/' + gameId,
      start: (apiBaseUrl || '') + '/user/' + userId + '/game/' + gameId + '/play',
      update: function (gamePlayId) {
        return (apiBaseUrl || '') + '/user/' + userId + '/game/' + gameId + '/play/' + gamePlayId;
      },
      settings: (apiBaseUrl || '') + '/user/' + userId + '/game/' + gameId + '/settings',
      purchase: (apiBaseUrl || '') + '/user/' + userId + '/game/' + gameId + '/shop',
      gameLeaderboard: function (topX) {
        return (apiBaseUrl || '') + '/user/' + userId + '/game-top-x/' + gameId + (topX ? '?topX=' + topX : '');
      },
      leaderboard: function (objectiveId, topX) {
        return (apiBaseUrl || '') + '/user/' + userId + '/objective-top-x/' + objectiveId + (topX ? '?topX=' + topX : '');
      },

      gameLeaderboardOld: (apiBaseUrl || '') + '/user/' + userId + '/game-top-x/' + gameId,
      leaderboardOld: function (objectiveId) {
        return (apiBaseUrl || '') + '/user/' + userId + '/objective-leaderboard/' + objectiveId;
      },

      user: function (userId) {
        return (apiBaseUrl || '') + '/user/' + userId;
      }
    };

  // what we actually return
  // ---


  var Api = {
    /**
     * Get the initial game data
     * @method init
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     * promise done {
     *    user: {
     *      id: (int),
     *      firstName: (string),
     *      lastName: (string),
     *    },
     *
     *    school: {
     *      id: (int),
     *      name: (string),
     *      country: (string: isocode)
     *    },
     *
     *    achievements: [{
     *      activityId: (int: maps to activity_id / festivals id.. {1...5}),
     *      medal: (string: null-No Medal, B-Bronze, S-Silver, G-Gold)
     *    }],
     *
     *    balance: (int),
     *
     *    assets: {},
     *
     *    env: {},
     *
     *    settings: {
     *      ?fullScreen: (boolean),
     *      ?mute: (boolean)
     *      ?showTips: (boolean)
     *      preferences: {}
     *    },
     *
     *    gameData: {}
     * }
     */
    init: function () {
      return internalApi.init();
    },

    /**
     * Update the user's game data
     * @method updateUserGameState
     * @param {Object} data The gameData object
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     */
    updateUserGameState: function (data) {
      return internalApi.updateUserGameState(data);
    },

    /**
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     * promise done {
     *   "id":
     *     {
     *       "userId": (int),
     *       "gameId": (int)
     *     },
     *   "highScore": (int),
     *   "lastScore": (int),
     *   "timePlayed: (int)
     * }
     */
    getAggregates: function () {
      return internalApi.getAggregates();
    },

    /**
     * Start a play
     * @method start
     *
     * @param {Number|array} levelId The level which has been played
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     */
    start: function (levelId) {
      return internalApi.start(levelId);
    },

    /**
     * Update a play
     * @method update
     *
     * @param {Object} data The save data {
     *    achievements: [{
     *      activityId: (int: maps to activity_id / festivals id.. {1...5}),
     *      medal: (string: null-No Medal, B-Bronze, S-Silver, G-Gold)
     *    }],
     *
     *    balance: (int),
     *
     *    env: {},
     *
     *    gameData: {}
     * }
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     */
    update: function (data) {
      return internalApi.update(data, ACTION_UPDATE);
    },

    /**
     * Finish a play
     * @method finish
     *
     * @param {Object} data The save data {
     *    achievements: [{
     *      activityId: (int: maps to activity_id / festivals id.. {1...5}),
     *      medal: (string: null-No Medal, B-Bronze, S-Silver, G-Gold)
     *    }],
     *
     *    balance: (int),
     *
     *    env: {},
     *
     *    gameData: {}
     * }
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     */
    finish: function (data) {
      return internalApi.update(data, ACTION_FINISH);
    },

    /**
     * Update the game settings (i.e. Mute, Avatar)
     * @method updateSettings
     *
     * @param {Object} data The settings data {
     *    ?fullScreen: (boolean),
     *    ?mute: (boolean)
     *    ?showTips: (boolean)
     *    preferences: {}
     * }
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     */
    updateSettings: function (data) {
      return internalApi.updateSettings(data);
    },

    /**
     * Fetch user information relative to the current user's credentials
     * @method getUser
     *
     * @param {Number} userId
     *
     * @returns {Promise}
     */
    getUser: function (userId) {
      return internalApi.getUser(userId);
    },

    /**
     * Purchase an item from the shop
     * @method purchaseItem
     *
     * @param {Number} itemId     The purchased item id
     * @param {Number} [quantity] The quantity of purchased items, Default: 1
     *
     * @returns {Promise} Done called when the AJAX request succeeds
     * promise done {
     *  goldBalance: (int)
     * }
     */
    purchaseItem: function (itemId, quantity) {
      return internalApi.purchaseItem(itemId, quantity);
    },

    /**
     * Get an external resource
     * @method getExternalResource
     *
     * @param {String} url          The url to fetch
     * @param {String} responseType The expected response type ('json'|'script'|null)
     *
     * @returns {Promise} Done called when the AJAX request succeeds/Script has finished loading
     * promise done Resource
     */
    getExternalResource: function (url, responseType) {
      return internalApi.getExternalResource(url, responseType);
    },

    /**
     * Create a new image
     *  - used to get round IE10 and crazy cross domain issues
     *
     * @param {String}    src
     * @param {Callback}  success
     * @param {Callback}  error
     *
     * @returns {Image}
     */
    createImage: function (src, success, error) {
      return internalApi.createImage(src, success, error);
    },

    /**
     * Fetch leaderboard data
     *
     * @param {Number}    objectiveId,
     * @param {Number}    topXResults
     *
     * @returns {Promise}
     */
    getLeaderboard: function (objectiveId, topXResults) {
      return internalApi.getLeaderboard(objectiveId, topXResults);
    },

    getLeaderboardOld: function (objectiveId) {
      return internalApi.getLeaderboardOld(objectiveId);
    },

    /**
     * Generate an object representation a Mangahigh achievement.
     *
     * @param {number} activityId
     * @param {string} medal One of either 'B', 'S', 'G'
     * @return {Object}
     */
    createAchievement: function (activityId, medal) {
      activityId = parseInt(activityId, 10);

      if (isNaN(activityId) || activityId === 0) {
        throw new Error('Invalid activityId supplied');
      }

      medal = medal.toUpperCase();

      if (medal !== 'B' && medal !== 'S' && medal !== 'G') {
        throw new Error('Invalid medal supplied');
      }

      return {
        activityId: parseInt(activityId, 10),
        medal: medal
      };
    }
  };

  // ---

  //
  /**
   * Rename promise to make things clearer for us
   *
   * @param Promise
   * @type promise.Promise
   * @readOnly
   */
  var Promise = promise.Promise,
    ajax = {
      get: promise.get,
      post: promise.post,
      put: promise.put,
      patch: promise.patch
    };

  /**
   * The update action
   *
   * @property ACTION_UPDATE
   * @type {String}
   * @readOnly
   * @static
   */
  var ACTION_UPDATE = 'update';

  /**
   * The finish action
   *
   * @property ACTION_FINISH
   * @type {String}
   * @readOnly
   * @static
   */
  var ACTION_FINISH = 'end';

  /**
   * A reponse type of json
   *
   * @property RESPONSE_JSON
   * @type String
   * @readOnly
   * @static
   */
  var RESPONSE_JSON = 'json';

  /**
   * A reponse type of a Javascript script file
   *
   * @property RESPONSE_SCRIPT
   * @type String
   * @readOnly
   * @static
   */
  var RESPONSE_SCRIPT = 'script';


  /**
   * Validates if a input is a integer
   *
   * @method isInt
   *
   * @param {Mixed} variabl
   *
   * @return {Boolean}
   */
  var isInt = function (variable) {
    return (typeof variable === 'number' && variable % 1 === 0);
  };

  // validate construction

  if (!isInt(gameId)) {
    throw new Error('An int of Game Id must be passed as the first variable');
  }

  if (userId && !isInt(userId)) {
    throw new Error('An int or null of User Id must be passed as the second variable');
  }

  // ---

  var validateBaseUrl = function (baseUrl) {
    if (!baseUrl && baseUrl !== null) {
      throw new Error('An string of the Base Url must be provided to the initialisation');
    }
  };

  validateBaseUrl(apiBaseUrl);
  validateBaseUrl(resourceBaseUrl);

  // ---

  /**
   * Wrapped in a object so we can use "this.*"
   *
   * @property GameApi
   * @type {Object}
   * @readOnly
   */
  var internalApi = {
    /**
     * A unique id representing this game play.
     * Is used so that we can correctly match Start, Update and End requests
     * in the backend
     *
     * @propery _gamePlayId
     * @type {Number}
     * @default null
     * @private
     */
    _gamePlayId: null,

    /**
     * The level id for this play. Stored here so that we don't have to keep
     * sending it from the game
     *
     * @propery _gamePlayId
     * @type {Number}
     * @default null
     * @private
     */
    _levelId: null,

    /**
     * The game play id is generated by the backend
     * If we haven't yet receieved this when we try and do an update then
     * we need to wait until this is returned
     *
     * @propery _startPromises
     * @type {Object}
     * @default {}
     * @private
     */
    _startPromises: {},

    /**
     * The index of the start promise related to this play,
     * incremented each time start is called
     *
     * @propery _gamePlayId
     * @type {Number}
     * @default 0
     * @private
     */
    _startPromiseIndex: 0,

    // ---

    init: function () {
      this._validateUser();

      var promise = new Promise();
      this._get(promise, ENDPOINTS.init, RESPONSE_JSON);
      return promise;
    },

    updateUserGameState: function (gameData) {
      this._validateUser();

      var promise = new Promise();

      if (typeof gameData !== 'object') {
        throw new Error('Must pass a gameData as an object to update user game state');
      }

      var patchData = [{
        op: 'replace',
        path: '/gameData',
        value: gameData
      }];

      this._patch(promise, ENDPOINTS.init, patchData, RESPONSE_JSON);
      return promise;
    },

    getAggregates: function () {
      this._validateUser();

      var promise = new Promise();
      this._get(promise, ENDPOINTS.aggregate, RESPONSE_JSON);
      return promise;
    },

    start: function (levelId) {
      this._validateUser();

      if (!isInt(levelId) && !Array.isArray(levelId)) {
        throw new Error('Must pass a valid levelId to start a play');
      }

      if ((Array.isArray(levelId) && levelId.length === 0)) {
        throw new Error('Must pass a valid levelId to start a play');
      }

      var promise = new Promise(),
        self = this,
        postData = {level: levelId};

      // if (this._gamePlayId) {
      //   postData.gamePlayId = this._gamePlayId;
      // }

      promise.then(function (error, data) {
        if (!error && data.gamePlayId) {
          self._gamePlayId = data.gamePlayId;
        }
      });

      this._post(promise, ENDPOINTS.start, postData, RESPONSE_JSON);

      this._levelId = levelId;

      ++this._startPromiseIndex;
      this._startPromises[this._startPromiseIndex] = promise;

      return promise;
    },

    update: function (data, action, levelId) {
      this._validateUser();

      if (!this._levelId) {
        throw new Error('Start must be called before calling update');
      }

      this._validateData(data);

      // all good - lets call the update method

      var self = this,
        promise = new Promise(),
        startPromiseIndex = this._startPromiseIndex;

      if (levelId === undefined) {
        data.level = this._levelId;
      }
      else {
        data.level = levelId;
      }

      data.action = action;

      this._startPromises[this._startPromiseIndex].then(function () {
        self._put(promise, ENDPOINTS.update(self._gamePlayId), data, RESPONSE_JSON);
        if (action === ACTION_FINISH) {
          delete self._startPromises[startPromiseIndex];
        }
      });

      return promise;
    },

    updateSettings: function (data) {
      this._validateUser();

      var promise = new Promise();
      this._put(promise, ENDPOINTS.settings, data, RESPONSE_JSON);
      return promise;
    },

    getUser: function (userId) {
      var promise = new Promise();

      this._get(promise, ENDPOINTS.user(userId), RESPONSE_JSON);
      return promise;
    },

    getExternalResource: function (url, responseType) {
      var promise = new Promise();

      if (responseType === RESPONSE_SCRIPT) {
        LazyLoad.js(url, function () {
          promise.done();
        });
      } else {
        this._get(promise, (resourceBaseUrl || '') + url, responseType);
      }

      return promise;
    },

    purchaseItem: function (itemId, quantity) {
      this._validateUser();

      if (!isInt(quantity)) {
        quantity = 1;
      }

      var promise = new Promise(),
        self = this,
        data = {
          itemId: itemId,
          quantity: quantity
        };

      self._post(promise, ENDPOINTS.purchase, data, RESPONSE_JSON);
      return promise;
    },

    /**
     * IE 10 doesn't respect CORS with new Image();
     * You get a security error when trying to getImageData()
     * This is a crazy workaround
     * - Load the image data using ajax
     * - Convert to base64
     * - add as data to the image
     * Because the image is now created locally there are no issues
     *
     * @see http://www.goodboydigital.com/ie-cross-domain-getpixeldata-hack/
     *
     * @param {String}    src
     * @param {Callback}  success
     * @param {Callback}  error
     *
     * @returns {Image}
     */
    createImage: function (src, success, error) {
      var image = new Image(),
          self = this;

      if (success) {
        image.onload = success;
      }

      if (error) {
        image.error = error;
      }

      if (navigator.appVersion.indexOf('MSIE 10') !== -1) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', src, true);
        xhr.responseType = 'arraybuffer';
        xhr.onload = function (e) {
          image.src = 'data:image/png;base64,' + self._base64ArrayBuffer(e.currentTarget.response);
        };
        xhr.send();
      } else {
        image.crossOrigin = '';
        image.src = src;
      }

      return image;
    },

    getLeaderboard: function (objectiveId, topX) {
      this._validateUser();

      var promise = new Promise(),
        self = this;

      if (!isInt(objectiveId)) {
        self._get(promise, ENDPOINTS.gameLeaderboard(topX), RESPONSE_JSON);
      } else {
        self._get(promise, ENDPOINTS.leaderboard(objectiveId, topX), RESPONSE_JSON);
      }

      return promise;
    },

    getLeaderboardOld: function (objectiveId) {
      this._validateUser();

      var promise = new Promise(),
        self = this;

      if (!isInt(objectiveId)) {
        self._get(promise, ENDPOINTS.gameLeaderboardOld, RESPONSE_JSON);
      } else {
        self._get(promise, ENDPOINTS.leaderboardOld(objectiveId), RESPONSE_JSON);
      }

      return promise;
    },

    // ---

    _validateUser: function () {
      if (!userId) {
        throw new Error('Must provide a user id to call this method');
      }
    },

    /**
     * Base 64 encode an array buffer
     *
     * @see https://gist.github.com/jonleighton/958841
     * @see http://stackoverflow.com/a/7372816
     *
     * @param {ArrayBuffer} arrayBuffer
     *
     * @returns {String}
     */
    _base64ArrayBuffer: function (arrayBuffer) {
      var base64    = '';
      var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

      var bytes         = new Uint8Array(arrayBuffer);
      var byteLength    = bytes.byteLength;
      var byteRemainder = byteLength % 3;
      var mainLength    = byteLength - byteRemainder;

      var a, b, c, d;
      var chunk;

      for (var i = 0; i < mainLength; i = i + 3) {
        chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];

        a = (chunk & 16515072) >> 18;
        b = (chunk & 258048)   >> 12;
        c = (chunk & 4032)     >>  6;
        d = chunk & 63;

        base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
      }

      if (byteRemainder === 1) {
        chunk = bytes[mainLength];

        a = (chunk & 252) >> 2;

        b = (chunk & 3)   << 4;

        base64 += encodings[a] + encodings[b] + '==';
      } else if (byteRemainder === 2) {
        chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];

        a = (chunk & 64512) >> 10;
        b = (chunk & 1008)  >>  4;

        c = (chunk & 15)    <<  2;

        base64 += encodings[a] + encodings[b] + encodings[c] + '=';
      }

      return base64;
    },

    _validateData: function (data) {
      if (!isInt(data.balance)) {
        throw new Error('Must pass a valid balance property to update a play');
      }

      if (!data.gameData || typeof data.gameData !== 'object') {
        throw new Error('Must pass a gameData object to update a play');
      }

      if (data.achievements) {
        for (var i = 0, length = data.achievements.length; i < length; ++i) {
          if (!isInt(data.achievements[i].activityId)) {
            throw new Error('Each achievement must have a valid activity id');
          }

          if (!data.achievements[i].hasOwnProperty('medal') || [null, 'B', 'S', 'G'].indexOf(data.achievements[i].medal) === -1) {
            throw new Error('Each achievement must have a valid medal');
          }
        }
      }
    },

    _get: function (p, url, responseType, attempt) {
      var self = this;

      if (typeof attempt === 'undefined') {
        attempt = 1;
      }

      ajax.get(url).then(function (error, data, xhr) {
        if (error && attempt < MAX_RETRY_ATTEMPTS) {
          self._get(p, url, responseType, ++attempt);
        } else {
          p.done(error, self._convertType(data, responseType), xhr);
        }
      });

      return p;
    },

    _post: function (p, url, postData, responseType, attempt) {
      var self = this,
        postString = JSON.stringify(postData),
        headers = {
          'X-Checksum': this._createChecksum(postString)
        };

      if (typeof attempt === 'undefined') {
        attempt = 1;
      }

      ajax.post(url, postString, headers).then(function (error, data, xhr) {
        if (error && attempt < MAX_RETRY_ATTEMPTS) {
          self._post(p, url, postData, responseType, ++attempt);
        } else {
          p.done(error, self._convertType(data, responseType), xhr);
        }
      });

      return p;
    },

    _put: function (p, url, putData, responseType, attempt) {
      var self = this,
        putString = JSON.stringify(putData),
        headers = {
          'X-Checksum': this._createChecksum(putString)
        };

      if (typeof attempt === 'undefined') {
        attempt = 1;
      }

      ajax.put(url, putString, headers).then(function (error, data, xhr) {
        if (error && attempt < MAX_RETRY_ATTEMPTS) {
          self._put(p, url, putData, responseType, ++attempt);
        } else {
          p.done(error, self._convertType(data, responseType), xhr);
        }
      });

      return p;
    },

    _patch: function (p, url, patchData, responseType, attempt) {
      var self = this,
        patchString = JSON.stringify(patchData),
        headers = {
          'X-Checksum': this._createChecksum(patchString)
        };

      if (typeof attempt === 'undefined') {
        attempt = 1;
      }

      ajax.patch(url, patchString, headers).then(function (error, data, xhr) {
        if (error && attempt < MAX_RETRY_ATTEMPTS) {
          self._patch(p, url, patchString, responseType, ++attempt);
        } else {
          p.done(error, self._convertType(data, responseType), xhr);
        }
      });

      return p;
    },

    _convertType: function (data, responseType) {
      if (responseType === RESPONSE_JSON && data) {
        return JSON.parse(data);
      }

      return data;
    },

    _createChecksum: function (data) {
      return md5(md5(data) + CHECKSUM_SALT);
    }
  };

  return Api;
};
