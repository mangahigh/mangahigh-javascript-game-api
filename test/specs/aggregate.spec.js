describe('Aggregate', function () {
  var Promise = promise.Promise;
  beforeEach(function() {
    var self = this;

    this.stubbedPromise = new Promise();
    this.ajaxCallCount = 0;
    this.postArguments = {};

    this.stubPut = sinon.stub(promise, 'get', function() {
      ++self.ajaxCallCount;
      self.postArguments = arguments;
      return self.stubbedPromise;
    });

    this.gameApi = new MangahighGameAPI(1, 2, null, null);
  });

  afterEach(function() {
    this.stubPost.restore();
    this.stubPut.restore();
  });

  // ---

  it('should call the correct method', function () {
    this.gameApi.getAggregates();

    expect(this.postArguments[0]).to.equal('/user/2/game-aggregate/1');
  });

  it('should attempt a post 3 times', function () {
    this.stubbedPromise = new Promise();

    this.gameApi.getAggregates();
    this.stubbedPromise.done(true);
    expect(this.ajaxCallCount).to.equal(3);
  });
});
