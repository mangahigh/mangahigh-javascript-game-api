describe('Start', function () {
  var Promise = promise.Promise;

  beforeEach(function() {
    var self = this;

    this.stubbedPromise = new Promise();
    this.ajaxCallCount = 0;
    this.postArguments = 0;

    this.stubPost = sinon.stub(promise, 'post', function() {
      ++self.ajaxCallCount;
      self.postArguments = arguments;
      return self.stubbedPromise;
    });

    this.stubPut = sinon.stub(promise, 'put', function() {
      ++self.ajaxCallCount;
      self.postArguments = arguments;
      return self.stubbedPromise;
    });

    this.gameApi = new MangahighGameAPI(1, 2, null, null);
  });

  afterEach(function() {
    this.stubPost.restore();
    this.stubPut.restore();
  });

  // ---

  it('should return a promise', function () {
    var p = this.gameApi.start(1);
    expect(p).to.be.instanceOf(Promise);
  });

  it('should call the correct url', function () {
    this.gameApi.start(44);
    expect(this.postArguments[0]).to.equal('/user/2/game/1/play');
    expect(JSON.parse(this.postArguments[1]).level).to.equal(44);
  });

  it('should set the levelId propery', function () {
    this.gameApi.start(1);
    this.gameApi.update({balance: 1, gameData: {}}, 'update');
  });

  it('should throw a error if not passed a level id', function () {
    var self = this,
      fn = function () {
        self.gameApi.start();
      };

      expect(fn).to.throw('Must pass a valid levelId to start a play');
  });

  it('should throw a error if not passed a integer level id', function () {
    var self = this,
      fn = function () {
        self.gameApi.start("1");
      };

      expect(fn).to.throw('Must pass a valid levelId to start a play');
  });


  it('should call done on the promise when the data is fetched', function () {
    var callCount = 0;
    var errorCallCount = 0;

    this.gameApi.start(1).then(function(error) {
      if(error) {
        ++errorCallCount;
      }
      ++callCount;
    });

    this.stubbedPromise.done(false, '{"gamePlayId": "asdf90kasd"}');

    expect(errorCallCount).to.equal(0);
    expect(callCount).to.equal(1);
  });

  it('should pass the error up when the promise fails', function () {
    var callCount = 0;
    var errorCallCount = 0;

    this.gameApi.start(1).then(function(error) {
      if(error) {
        ++errorCallCount;
      }
      ++callCount;
    });

    this.stubbedPromise.done(true);

    expect(errorCallCount).to.equal(1);
    expect(callCount).to.equal(1);
  });

  it('should set the gamePlayId on success', function () {
    this.gameApi.start(123);
    this.stubbedPromise.done(false, '{"gamePlayId": "asdf90kasd"}');
    this.gameApi.update({balance: 1, gameData: {}}, 'update');
  });

  it('should attempt a post 3 times', function () {
    this.gameApi.start(1);

    this.stubbedPromise.done(true);
    expect(this.ajaxCallCount).to.equal(3);
  });
});