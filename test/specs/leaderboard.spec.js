describe('Leaderboard', function () {
  var Promise = promise.Promise;

  beforeEach(function() {
    var self = this;

    this.stubbedPromise = new Promise();
    this.ajaxCallCount = 0;
    this.getUrl = '';

    this.stubGet = sinon.stub(promise, 'get', function() {
      ++self.ajaxCallCount;
      self.getUrl = arguments[0];
      return self.stubbedPromise;
    });

    this.gameApi = new MangahighGameAPI(1, 2, null, null);
  });

  afterEach(function() {
    this.stubGet.restore();
  });

  // ---

  it('should return a promise', function () {
    var p = this.gameApi.getLeaderboard(1);
    expect(p).to.be.instanceOf(Promise);
  });

  it('should call the correct url with a level id', function () {
    this.gameApi.getLeaderboard(44);
    expect(this.getUrl).to.equal('/user/2/objective-top-x/44');
  });

  it('should call the correct url without a level id', function () {
    this.gameApi.getLeaderboard();
    expect(this.getUrl).to.equal('/user/2/game-top-x/1');
  });

  it('should call done on the promise when the data is fetched', function () {
    var callCount = 0;
    var errorCallCount = 0;

    this.gameApi.getLeaderboard(1).then(function(error) {
      if(error) {
        ++errorCallCount;
      }
      ++callCount;
    });

    this.stubbedPromise.done(false, '');

    expect(errorCallCount).to.equal(0);
    expect(callCount).to.equal(1);
  });

  it('should pass the error up when the promise fails', function () {
    var callCount = 0;
    var errorCallCount = 0;

    this.gameApi.getLeaderboard(1).then(function(error) {
      if(error) {
        ++errorCallCount;
      }
      ++callCount;
    });

    this.stubbedPromise.done(true);

    expect(errorCallCount).to.equal(1);
    expect(callCount).to.equal(1);
  });

});
