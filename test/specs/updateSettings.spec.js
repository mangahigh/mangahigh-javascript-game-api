describe('Update Settings', function () {
  var Promise = promise.Promise;
  beforeEach(function() {
    var self = this;

    this.stubbedPromise = new Promise();
    this.ajaxCallCount = 0;
    this.postArguments = {};

    this.stub = sinon.stub(promise, 'put', function() {
      ++self.ajaxCallCount;
      self.postArguments = arguments;
      return self.stubbedPromise;
    });

    this.gameApi = new MangahighGameAPI(1, 2, null, null);
  });

  afterEach(function() {
    this.stub.restore();
  });

  // ---

  it('should attempt a put 3 times', function () {
    this.gameApi.updateSettings({});
    this.stubbedPromise.done(true);
    expect(this.ajaxCallCount).to.equal(3);
  });

  it('should post to the correct url', function () {
    var data = {
      settingValue: false
    };

    this.gameApi.updateSettings(data);
    expect(this.postArguments[0]).to.equal('/user/2/game/1/settings');
    expect(JSON.parse(this.postArguments[1]).settingValue).to.equal(false);
  });
});
