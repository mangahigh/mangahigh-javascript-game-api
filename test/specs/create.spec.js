describe('Create', function () {
  it('should be instatiable', function () {
    var gameApi = new MangahighGameAPI(1, 2, null, null);
    expect(gameApi).to.be.instanceOf(Object);
  });

  it('should return a new instance if called twice', function () {
    var gameApi = new MangahighGameAPI(1, 2, null, null);
    var gameApi2 = new MangahighGameAPI(1, 2, null, null);

    expect(gameApi).to.not.equal(gameApi2);
  });

  it('should throw an error if game id is invalid', function () {
      expect(function () {new MangahighGameAPI('test', 2, null, null);}).to.throw('An int of Game Id must be passed as the first variable');
  });

  it('should throw an error if user id is invalid', function () {
      expect(function () {new MangahighGameAPI(2333233, true, null, null);}).to.throw('An int or null of User Id must be passed as the second variable');
  });

  it('should throw an error if resource base url is invalid', function () {
      expect(function () {new MangahighGameAPI(2333233, 3, null, '');}).to.throw('An string of the Base Url must be provided to the initialisation');
  });
});
