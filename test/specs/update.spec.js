describe('Update', function () {
  var Promise = promise.Promise;
  beforeEach(function() {
    var self = this;

    this.stubbedPromise = new Promise();
    this.ajaxCallCount = 0;
    this.postArguments = {};

    this.stubPost = sinon.stub(promise, 'post', function() {
      ++self.ajaxCallCount;
      self.postArguments = arguments;
      return self.stubbedPromise;
    });

    this.stubPut = sinon.stub(promise, 'put', function() {
      ++self.ajaxCallCount;
      self.postArguments = arguments;
      return self.stubbedPromise;
    });


    this.gameApi = new MangahighGameAPI(1, 2, null, null);
  });

  afterEach(function() {
    this.stubPost.restore();
    this.stubPut.restore();
  });

  // ---

  it('should fail if start has not been called', function () {
    var self = this,
      fn = function () {
        self.gameApi.update({timePlayed: 1, state: {}}, 'update');
      };

    expect(fn).to.throw('Start must be called before calling update');
  });


  it('should validate the data has a valid balance property', function() {
      var self = this,
          fn = function () {
            self.gameApi.update({gameData: {}}, 'update');
          };

    this.gameApi.start(1);
    expect(fn).to.throw('Must pass a valid balance property to update a play');
  });

  it('should validate the data has a valid gameData property', function() {
      var self = this,
          fn = function () {
            self.gameApi.update({balance: 1}, 'update');
          };

    this.gameApi.start(1);
    expect(fn).to.throw('Must pass a gameData object to update a play');
  });

  it('should validate that if data has an achievements property that each item has a valid activity id', function () {
      var self = this,
          fn = function () {
            self.gameApi.update({balance: 1, gameData: {}, achievements: [{activityId:'test', medal:'B'}]}, 'update');
          };

    this.gameApi.start(1);
    expect(fn).to.throw('Each achievement must have a valid activity id');
  });

  it('should validate that if data has an achievements property that each item has a valid medal', function () {
      var self = this,
          fn = function () {
            self.gameApi.update({balance: 1, gameData: {}, achievements: [{activityId:1, medal:'F'}]}, 'update');
          };

    this.gameApi.start(1);
    expect(fn).to.throw('Each achievement must have a valid medal');
  });

  it('should add levelId and action to the data', function () {
    this.gameApi.start(43);
    this.stubbedPromise.done(false, '{"gamePlayId": "asdf90kasd"}');

    this.gameApi.update({balance: 1, gameData: {}}, 'update');

    expect(this.postArguments[0]).to.equal('/user/2/game/1/play/asdf90kasd');
    expect(JSON.parse(this.postArguments[1]).level).to.equal(43);
    expect(JSON.parse(this.postArguments[1]).action).to.equal('update');
  });

  it('should attempt a post 3 times', function () {
    this.gameApi.start(1);
    this.stubbedPromise.done(false, '{"gamePlayId": "asdf90kasd"}');
    this.stubbedPromise = new Promise();

    this.gameApi.update({balance: 1, gameData: {}}, 'update');
    this.stubbedPromise.done(true);
    expect(this.ajaxCallCount).to.equal(4);
  });

  it('should track the correct start', function () {
    this.gameApi.start(1);
    this.stubbedPromise.done(false, '{"gamePlayId": "asdf90kasd"}');
    this.gameApi.update({balance: 1, gameData: {}}, 'update');

    expect(this.postArguments[0]).to.equal('/user/2/game/1/play/asdf90kasd');
    expect(JSON.parse(this.postArguments[1]).level).to.equal(1);
    expect(JSON.parse(this.postArguments[1]).action).to.equal('update');

    this.gameApi.start(2);
    this.gameApi.update({balance: 1, gameData: {}}, 'update');

    expect(this.postArguments[0]).to.equal('/user/2/game/1/play/asdf90kasd');
    expect(JSON.parse(this.postArguments[1]).level).to.equal(2);
    expect(JSON.parse(this.postArguments[1]).action).to.equal('update');
  });

  it('should create the correct headers', function () {
    this.gameApi.start(43);
    this.stubbedPromise.done(false, '{"gamePlayId": "asdf90kasd"}');

    this.gameApi.update({balance: 1, gameData: {}}, 'update');

    expect(this.postArguments[2]['X-Checksum']).to.equal('ce1d14b97cae48080dbc6316d234ab3d');
  });
});
