describe('Get External Resource', function () {
  var Promise = promise.Promise;
  beforeEach(function() {
    var self = this;

    this.stubbedPromise = new Promise();
    this.ajaxCallCount = 0;
    this.postArguments = {};

    this.stub = sinon.stub(promise, 'get', function() {
      ++self.ajaxCallCount;
      self.postArguments = arguments;
      return self.stubbedPromise;
    });

    this.gameApi = null;
  });

  afterEach(function() {
    this.stub.restore();
  });

  // ---

  it('should attempt a get 3 times', function () {
    this.gameApi = new MangahighGameAPI(1, 2, null, null);
    this.gameApi.getExternalResource('/config');
    this.stubbedPromise.done(true);
    expect(this.ajaxCallCount).to.equal(3);
  });

  it('should post to the correct url', function () {
    this.gameApi = new MangahighGameAPI(1, 2, null, null);
    this.gameApi.getExternalResource('/config');
    expect(this.postArguments[0]).to.equal('/config');
  });

  it('should prepend the Base Url', function () {
    this.gameApi = new MangahighGameAPI(1, 2, null, 'http://example.com');
    this.gameApi.getExternalResource('/config');
    expect(this.postArguments[0]).to.equal('http://example.com/config');
  });
});
