describe('Init', function () {
  var Promise = promise.Promise;

  beforeEach(function() {
    var self = this;

    this.stubbedPromise = new Promise();
    this.ajaxCallCount = 0;

    this.stub = sinon.stub(promise, 'get', function() {
      ++self.ajaxCallCount;
      return self.stubbedPromise;
    });

    this.gameApi = new MangahighGameAPI(1, 2, null, null);
  });

  afterEach(function() {
    this.stub.restore();
  });

  // ---

  it('should return a promise', function () {
    var p = this.gameApi.init();
    expect(p).to.be.instanceOf(Promise);
  });

  it('should call done on the promise when the data is fetched', function () {
    var callCount = 0;
    var errorCallCount = 0;

    this.gameApi.init().then(function(error) {
      if(error) {
        ++errorCallCount;
      }
      ++callCount;
    });

    this.stubbedPromise.done(false);

    expect(errorCallCount).to.equal(0);
    expect(callCount).to.equal(1);
  });

  it('should pass the error up when the promise fails', function () {
    var callCount = 0;
    var errorCallCount = 0;

    this.gameApi.init().then(function(error) {
      if(error) {
        ++errorCallCount;
      }
      ++callCount;
    });

    this.stubbedPromise.done(true);

    expect(errorCallCount).to.equal(1);
    expect(callCount).to.equal(1);
  });

  it('should pass up the result on promise success', function () {
    var data = '{"timePlayed": 1}';
    var recievedData = null;

    this.gameApi.init().then(function(error, data) {
      recievedData = data;
    });

    this.stubbedPromise.done(false, data);

    expect(recievedData.timePlayed).to.equal(1);
  });

  it('should attempt a get 3 times', function () {
    this.gameApi.init();
    this.stubbedPromise.done(true);
    expect(this.ajaxCallCount).to.equal(3);
  });
});
